import csv
import copy
from pprint import pprint
import json


def open_files(file_name):
    result = []
    with open(file_name, 'r') as f:
        reader = csv.reader(f, delimiter=";")
        counter = 0
        for row in reader:
            if counter == 0:
                key = row
                counter += 1
                continue
            for element in range(len(row)):
                try:
                    row[element] = json.loads(row[element])
                except json.decoder.JSONDecodeError:
                    continue

            result.append(dict(zip(key, row)))
            counter += 1

    return result


friends = open_files('friends.csv')
tags = open_files('tags.csv')
users = open_files('user.csv')

result_file = copy.deepcopy(users)
for user in result_file:
    for friend in friends:
        if friend["_id"] == user["_id"]:
            user["friends"] = friend["friends"]
    for tag in tags:
        if tag["_id"] == user["_id"]:
            user["tags"] = tag["tags"]


js_result_file = json.dumps(result_file)
with open("result_file.json", "w") as f:
    f.write(js_result_file)

pprint(result_file)
